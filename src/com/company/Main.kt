package com.company

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.io.IOException

object Main {

    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        val baseUrl = "https://play.google.com/store/apps/category/FINANCE/collection/topselling_paid"
        Jsoup.connect(baseUrl).timeout(0).get()
                .getElementsByClass("card-click-target")
                .map { element -> "https://play.google.com/" + element.attr("href") }
                .forEach { url ->
                    val document = Jsoup.connect(url).timeout(0).get()
                    println("Title: ${document.getValueForClass("id-app-title")}")
                    println("Description: ${document.queryCss("[itemprop='description']")}")
                    println("Changelog: ${document.getValueForClass("recent-change")}")
                    println("Score: ${document.getValueForClass("score")}")
                    println("Ratings: ${document.getValueForClass("rating-count")}")
                    println("Rating 4: ${document.getValueForClass("rating-bar-container fivee")}")
                    println("Rating 5: ${document.getValueForClass("rating-bar-container four")}")
                    println()
                }
    }

    private fun Document.getValueForClass(clazz: String): String {
        if (this.getElementsByClass(clazz).size == 0) return ""
        return this.getElementsByClass(clazz)[0].text()
    }
    private fun Document.queryCss(selector: String): String = this.select(selector).text()

}
